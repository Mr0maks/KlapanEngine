//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef VIEWPORT_PANEL_NAMES_H
#define VIEWPORT_PANEL_NAMES_H
#ifdef _WIN32
#pragma once
#endif


// default panel name definitions
#define PANEL_ALL			"all"		// all current panels
#define PANEL_ACTIVE		"active"	// current active panel			

#define PANEL_SCOREBOARD	"scores"
#define PANEL_OVERVIEW		"overview"
#define PANEL_CLASS			"class"
#define PANEL_TEAM			"team"
#define PANEL_SPECGUI		"specgui"	// passive spectator elements (top/bottom bars)
#define PANEL_INFO			"info"
#define PANEL_BUY			"buy"
#define PANEL_NAV_PROGRESS	"nav_progress"
#define PANEL_INTRO			"intro"
#define PANEL_RADIO_MENU	"radio_menu"


#define PANEL_COMMENTARY_MODELVIEWER	"commentary_modelviewer"

#endif // VIEWPORT_PANEL_NAMES_H
