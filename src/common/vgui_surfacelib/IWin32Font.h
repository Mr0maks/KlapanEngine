//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef IWIN32FONT_H
#define IWIN32FONT_H
#ifdef _WIN32
#pragma once
#endif

#if !defined( _X360 )
#define WIN32_LEAN_AND_MEAN
#define OEMRESOURCE
#include <windows.h>
#endif
#ifdef GetCharABCWidths
#undef GetCharABCWidths
#endif

//-----------------------------------------------------------------------------
// Purpose: encapsulates a windows font
//-----------------------------------------------------------------------------
class IWin32Font
{
public:
	// creates the font from windows.  returns false if font does not exist in the OS.
	virtual bool Create( const char* windowsFontName, int tall, int weight, int blur, int scanlines, int flags ) = 0;
	virtual bool CreateFromMemory( const char* windowsFontName, void* data, int size, int tall, int weight, int blur, int scanlines, int flags ) = 0;

	// writes the char into the specified 32bpp texture
	virtual void GetCharRGBA( wchar_t ch, int rgbaWide, int rgbaTall, unsigned char* rgba ) = 0;

	// returns true if the font is equivalent to that specified
	virtual bool IsEqualTo( const char* windowsFontName, int tall, int weight, int blur, int scanlines, int flags ) = 0;

	// returns true only if this font is valid for use
	virtual bool IsValid() = 0;

	// gets the abc widths for a character
	virtual void GetCharABCWidths( int ch, int& a, int& b, int& c ) = 0;

	// set the font to be the one to currently draw with in the gdi
	virtual void SetAsActiveFont( HDC hdc ) = 0;

	// returns the height of the font, in pixels
	virtual int GetHeight() = 0;

	// returns requested height of font.
	virtual int GetHeightRequested() = 0;

	// returns the ascent of the font, in pixels (ascent=units above the base line)
	virtual int GetAscent() = 0;

	// returns the maximum width of a character, in pixels
	virtual int GetMaxCharWidth() = 0;

	// returns the flags used to make this font
	virtual int GetFlags() = 0;

	// returns true if this font is underlined
	virtual bool GetUnderlined() = 0;

	virtual const char* GetName() = 0;
	virtual const char* GetFamilyName() = 0;

	virtual bool HasChar( wchar_t wch ) = 0;

	// gets the width of ch given its position around before and after chars
	virtual void GetKernedCharWidth( wchar_t ch, wchar_t chBefore, wchar_t chAfter, float& wide, float& abcA, float& abcC ) = 0;
};

#endif // IWIN32FONT_H
